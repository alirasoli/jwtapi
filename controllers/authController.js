const passport = require('passport');
const { check, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');
var User = require('../models/User');

exports.validateRegister = [
  check('email').isEmail().normalizeEmail().not().isEmpty(),
  check('name').not().isEmpty(),
  check('password').not().isEmpty(),
  sanitizeBody('name'),
  sanitizeBody('email')
];

exports.validateLogin = [
  check('email').isEmail().normalizeEmail().not().isEmpty(),
  check('password').not().isEmpty()
];

exports.register = (req, res) => {
  const errors = validationResult(req);
  if(!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  User.findOne({ email: req.body.email }, (err, user) => {
    if(err) return res.status(400).send(err);
    if(user) {
      return res.status(400).json({ success: false, message: 'User already exists'});
    } else {
      var newUser = new User();
      newUser.name = req.body.name;
      newUser.email = req.body.email;

      newUser.setPassword(req.body.password);

      newUser.save((err) => {
        if(err) return res.status(400).send(err);
        var token;
        token = newUser.generateJWT();
        res.status(200);
        res.json({
          token
        });
      });
    }
  });
};

exports.login = (req, res) => {
  const errors = validationResult(req);
  if(!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  passport.authenticate('local', (err, user, info) => {
    if(err) {
      res.status(404).json(err);
      return;
    }

    if(user) {
      token = user.generateJWT();
      res.status(200);
      res.json({
        token
      });
    } else {
      res.status(401).json(info);
    }
  })(req, res);
};

// "bearer "+ token
exports.auth = passport.authenticate('jwt', { session: false});