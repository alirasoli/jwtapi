const express = require('express');
const router = express.Router();
const authController = require('../../controllers/authController');

router.use('/auth', require('./auth'));
router.use('/user', authController.auth, require('./user'));

module.exports = router;