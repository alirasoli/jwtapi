const express = require('express');
const router = express.Router();
const ExtractJwt = require('passport-jwt').ExtractJwt;

router.get('/', (req, res) => {
  res.send("Hi " + req.user.name + " I am from cloud!");
});

module.exports = router;