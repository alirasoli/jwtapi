const express = require('express');
const routes = require('./routes/index');
const mongoose = require('mongoose');
const passport = require('passport');
const User = require('./models/User');
const bodyParser = require('body-parser');
const app = express();

require('dotenv').config();
require('./config/passport');

app.use(passport.initialize());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

mongoose.connect(process.env.DB, { useNewUrlParser: true }, (err) => {
  if(err) {
    console.log('oh shit!!!!🤦‍♂️🤦‍♂️ db failed to connect');
  } else {
    console.log(`MongoDB connected. DB name: ${process.env.DB.split('/').pop()}`);
  }
});

app.get('/', (req, res) => { res.send('welecome to api!')});

app.use('/', routes);

const server = app.listen(process.env.PORT || 5555, () => {
  console.log('Server is listening on port ' + server.address().port);
});