const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongodbErrorHandler = require('mongoose-mongodb-errors');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');


const userSchema = new Schema({
  name: {type: String, required: 'Please supply a name', trim: true},
  email: {type: String, required: 'Please supply a username', trim: true},
  hash: String,
  salt: String,
  resetPasswordToken: String,
  resetPasswordExpires: Date,
});

userSchema.plugin(mongodbErrorHandler);

userSchema.methods.setPassword = function(password) {
  this.salt = bcrypt.genSaltSync(10);
  this.hash = bcrypt.hashSync(password, this.salt);
};

userSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.hash);
};

userSchema.methods.generateJWT = function() {
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);

  return jwt.sign({
    _id: this._id,
    name: this.name,
    email: this.email,
    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 1000)
  }, process.env.SECRET);
};

module.exports = mongoose.model('User', userSchema);