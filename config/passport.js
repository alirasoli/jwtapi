const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/User');

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  },
  function(email, password, done) {
    User.findOne({ email: email }, (err, user) => {
      if(err) { return done(err); }

      if(!user) {
        return done(null, false, {
          message: 'User not found'
        });
      }

      if(!user.validPassword(password)) {
        return done(null, false, {
          message: 'Password is wrong'
        });
      }

      return done(null, user);
    });
  }
));

let opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.SECRET;
passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
  User.findById(jwt_payload._id, (err, user) => {
    if(err) return done(err, false);
    if(user) return done(null, user);
    else return done(null, false);
  });
}));